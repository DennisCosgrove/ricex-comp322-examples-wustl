package module1.unit1.topic1;

import static edu.wustl.cse231s.rice.Habanero.async;
import static edu.wustl.cse231s.rice.Habanero.finish;

import java.util.Random;

/**
 * Based on:
 * http://www.cs.rice.edu/~vs3/hjlib/code/course-materials/demo-files/ReciprocalArraySum.java
 * by Vivek Sarkar (vsarkar@rice.edu)
 * 
 * @author Dennis Cosgrove (cosgroved@wustl.edu)
 */
public class ReciprocalArraySum {
	public static void main(final String[] args) {
		final int N = 100_000_000;
		double[] X = new double[N];
		Random myRand = new Random();
		for (int i = 0; i < N; i++) {
			X[i] = myRand.nextDouble();
			if (X[i] == 0.0) {
				i--;
			}
		}

		for (int numRun = 0; numRun < 5; numRun++) {
			System.out.printf("Run %d\n", numRun);
			seqArraySum(X);
			parArraySum(X);
		}
	}

	private static double seqArraySum(final double[] X) {
		long startTime = System.nanoTime();
		
		
		double[] sums = { 0, 0 };
		// Compute sum of lower half of array
		for (int i = 0; i < X.length / 2; i++) {
			sums[0] += 1.0/X[i];
		}
		// Compute sum of upper half of array
		for (int i = X.length / 2; i < X.length; i++) {
			sums[1] += 1.0/X[i];
		}
		// Combine sum1 and sum2
		double sum = sums[0] + sums[1];

		
		long timeInNanos = System.nanoTime() - startTime;
		printResults("seqArraySum", timeInNanos, sum);
		return sum;
	}

	private static double parArraySum(final double[] X) {
		long startTime = System.nanoTime();
		
		
		double[] sums = { 0, 0 };
		finish(() -> {
			async(() -> {
				// Compute sum of lower half of array
				for (int i = 0; i < X.length / 2; i++) {
					sums[0] += 1.0/X[i];
				}
			});
			// Compute sum of upper half of array
			for (int i = X.length / 2; i < X.length; i++) {
				sums[1] += 1.0/X[i];
			}
		});
		// Combine sum1 and sum2
		double sum = sums[0] + sums[1];
		
		
		long timeInNanos = System.nanoTime() - startTime;
		printResults("parArraySum", timeInNanos, sum);
		return sum;
	}

	private static void printResults(final String name, final long timeInNanos, final double sum) {
		System.out.printf("  %s completed in %8.3f milliseconds, with sum = %8.5f \n", name, timeInNanos / 1e6, sum);
	}
}