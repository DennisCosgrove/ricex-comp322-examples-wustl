package module1.unit1.topic3;

import static edu.wustl.cse231s.rice.classic.HabaneroClasssic.async;
import static edu.wustl.cse231s.rice.classic.HabaneroClasssic.doWork;
import static edu.wustl.cse231s.rice.classic.HabaneroClasssic.dumpStatistics;
import static edu.wustl.cse231s.rice.classic.HabaneroClasssic.finish;
import static edu.wustl.cse231s.rice.classic.HabaneroClasssic.launchHabaneroApp;

import edu.rice.hj.api.SuspendableException;
import edu.rice.hj.runtime.config.HjSystemProperty;
import edu.wustl.cse231s.rice.classic.HabaneroClasssic;

/**
 * Reads in two strings, the pattern and the input text, and searches for the pattern in the input text.
 *
 *  % java Search abracadabra abacadabrabracabracadabrabrabracad
 *  text:    abacadabrabracabracadabrabrabracad 
 *  pattern:               abracadabra          
 *
 *  % java Search rab abacadabrabracabracadabrabrabracad
 *  text:    abacadabrabracabracadabrabrabracad 
 *  pattern:         rab                         
 * 
 *  % java Search rabrabracad abacadabrabracabracadabrabrabracad
 *  text:    abacadabrabracabracadabrabrabracad
 *  pattern:                        rabrabracad
 *
 *  % java Search bcara abacadabrabracabracadabrabrabracad
 *  text:    abacadabrabracabracadabrabrabracad 
 *  pattern:                                   bcara
 * 
 *  % java Search abacad abacadabrabracabracadabrabrabracad
 *  text:    abacadabrabracabracadabrabrabracad
 *  pattern: abacad
 *
 * ported from http://algs4.cs.princeton.edu/53substring/Brute.java.html
 *
 * @author Vivek Sarkar (vsarkar@rice.edu)
 */
public class Search {
	private static final String DEFAULT_PATTERN = "rabrabracad";
	private static final String DEFAULT_TEXT = "abacadabrabracabracadababacadabrabracabracadabrabrabracad";

	public static void main(final String[] args) {

		String pat = args.length >= 1 ? args[0] : DEFAULT_PATTERN;
		String txt = args.length >= 2 ? args[1] : DEFAULT_TEXT;

		char[] pattern = pat.toCharArray();
		char[] text = txt.toCharArray();

		System.out.println("text:    " + txt);
		System.out.println("pattern: " + pat);

		HjSystemProperty.abstractMetrics.set(true);
		launchHabaneroApp(() -> {
			boolean seqFound = searchSeq(pattern, text);
			System.out.println("Pattern found by sequential algorithm: " + seqFound);
		}, () -> {
			dumpStatistics();
		} );

		launchHabaneroApp(() -> {
			boolean parFound = searchPar(pattern, text);
			System.out.println("Pattern found by parallel algorithm: " + parFound);
		}, () -> {
			dumpStatistics();
		} );
	}

    /**
     * <p>searchSeq.</p>
     *
     * @param pattern an array of char.
     * @param text    an array of char.
     * @return a boolean.
     */
	public static boolean searchSeq(final char[] pattern, final char[] text) {
		final int M = pattern.length;
		final int N = text.length;
		final boolean[] found = { false };

		for (int i = 0; i <= N - M; i++) {
			int j;
			for (j = 0; j < M; j++) {
				doWork(1); // Count each char comparison as 1 unit of work
				if (text[i + j] != pattern[j]) {
					break;
				}
			}
			if (j == M) {
				found[0] = true;
			}
		}
		return found[0];
	}

    /**
     * <p>searchPar.</p>
     *
     * @param pattern an array of char.
     * @param text    an array of char.
     * @return a boolean.
     */
    public static boolean searchPar(final char[] pattern, final char[] text) throws SuspendableException {
        final int M = pattern.length;
        final int N = text.length;
        final boolean[] found = {false};
        finish(() -> {
            for (int i = 0; i <= N - M; i++) {
                final int ii = i;
                async(() -> {
                    int j;
                    for (j = 0; j < M; j++) {
                        doWork(1); // Count each char comparison as 1 unit of work
                        if (text[ii + j] != pattern[j]) {
                            break;
                        }
                    }
                    if (j == M) {
                        found[0] = true;
                    }
                });
            }
        });
        return found[0];
    }
}