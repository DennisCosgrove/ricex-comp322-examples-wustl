package module1.unit2.topic1;

import static edu.wustl.cse231s.rice.Habanero.*;

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import edu.wustl.cse231s.rice.Habanero;
import edu.wustl.cse231s.rice.chained.HabaneroChainedExceptionUtils;

/**
 * ReciprocalArraySum --- Computing the sum of reciprocals of array elements with 2-way parallelism
 * <p>
 * The goal of this example program is to create an array of n random int's, and compute the sum of their reciprocals in
 * two ways: 1) Sequentially in method seqArraySum() 2) In parallel using two tasks in method parArraySum() The
 * profitability of the parallelism depends on the size of the array and the overhead of async creation.
 * <p>
 * Your assignment is to use two-way parallelism in method parArraySum() to obtain a smaller execution time than
 * seqArraySum()
 *
 * @author Vivek Sarkar (vsarkar@rice.edu)
 */
public class ReciprocalArraySumFutures {
	public static final int DEFAULT_N = 100_000_000;

	public static void main(final String[] argv) throws Exception {
		// Initialization
		int n = argv.length > 0 ? Integer.parseInt(argv[0]) : DEFAULT_N;
		double[] X = new double[n];
		final Random myRand = new Random(n);

		for (int i = 0; i < n; i++) {
			X[i] = myRand.nextInt(n);
			if (X[i] == 0.0) {
				i--;
			}
		}

		for (int numRun = 0; numRun < 5; numRun++) {
			System.out.printf("Run %d\n", numRun);
			seqArraySum(X);
			parArraySum(X);
			HabaneroChainedExceptionUtils.finish(() -> {
				parArraySumFutures(X);
			});
		}
	}

	private static double seqArraySum(final double[] X) {
		final long startTime = System.nanoTime();

		double[] sums = { 0, 0 };
		// Compute sum of lower half of array
		for (int i = 0; i < X.length / 2; i++) {
			sums[0] += 1.0 / X[i];
		}
		// Compute sum of upper half of array
		for (int i = X.length / 2; i < X.length; i++) {
			sums[1] += 1.0 / X[i];
		}
		// Combine sum1 and sum2
		final double sum = sums[0] + sums[1];

		final long timeInNanos = System.nanoTime() - startTime;
		printResults("seqArraySum", timeInNanos, sum);
		return sum;
	}

	private static double parArraySum(final double[] X) {
		final long startTime = System.nanoTime();

		double[] sums = { 0, 0 };
		finish(() -> {
			async(() -> {
				// Compute sum of lower half of array
				for (int i = 0; i < X.length / 2; i++) {
					sums[0] += 1.0 / X[i];
				}
			});
			// Compute sum of upper half of array
			for (int i = X.length / 2; i < X.length; i++) {
				sums[1] += 1.0 / X[i];
			}
		});
		// Combine sum1 and sum2
		final double sum = sums[0] + sums[1];

		final long timeInNanos = System.nanoTime() - startTime;
		printResults("parArraySum", timeInNanos, sum);
		return sum;
	}

	public static double parArraySumFutures(final double[] X) throws ExecutionException, InterruptedException {
		final long startTime = System.nanoTime();

		Future<Double> sumLowerFuture = future(() -> {
			// Return sum of lower half of array
			double lowerSum = 0;
			for (int i = 0; i < X.length / 2; i++) {
				lowerSum += 1 / X[i];
			}
			return lowerSum;
		});
		Future<Double> sumUpperFuture = future(() -> {
			// Return sum of upper half of array
			double upperSum = 0;
			for (int i = X.length / 2; i < X.length; i++) {
				upperSum += 1 / X[i];
			}
			return upperSum;
		});

		// Combine sum1 and sum2
		final double sum = sumLowerFuture.get() + sumUpperFuture.get();
		final long timeInNanos = System.nanoTime() - startTime;
		printResults("parArraySumFutures", timeInNanos, sum);
		return sum;
	}

	private static void printResults(final String name, final long timeInNanos, final double sum) {
		System.out.printf("  %18s completed in %8.3f milliseconds, with sum = %8.5f \n", name, timeInNanos / 1e6, sum);
	}
}