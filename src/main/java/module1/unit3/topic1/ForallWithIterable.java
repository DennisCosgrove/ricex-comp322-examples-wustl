package module1.unit3.topic1;

import static edu.wustl.cse231s.rice.Habanero.async;
import static edu.wustl.cse231s.rice.Habanero.finish;
import static edu.wustl.cse231s.rice.Habanero.forall;

import java.util.Arrays;
import java.util.List;

/**
 * <p>ForallWithIterable class.</p>
 *
 * @author <a href="http://shams.web.rice.edu/">Shams Imam</a> (shams@rice.edu)
 */
public class ForallWithIterable {

    public static void main(final String[] args) {

        final List<Integer> myList = Arrays.asList(1, 2, 3, 4, 5);

            useFinishAndAsync(myList);
            useForall(myList);

    }

    private static void useFinishAndAsync(final List<Integer> myList) {
        System.out.println("useFinishAndAsync: ");
        finish(() -> {
            for (final Integer item : myList) {
                async(() -> {
                    System.out.printf("  Executing item-%d \n", item);
                });
            }
        });
    }

    private static void useForall(final List<Integer> myList) {
        System.out.println("useForall: ");
        forall(myList, (item) -> {
            System.out.printf("  Executing item-%d \n", item);
		});
	}
}